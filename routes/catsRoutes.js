module.exports = app => {
    const cats = require("../controllers/catsControllers.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", cats.create);
  
    // Retrieve all Tutorials
    router.get("/", cats.findAll);
  
    // Retrieve all Tutorials with given condition
    router.get("/:cond", cats.findWithCondition);
  
    // Retrieve a single Tutorial with id
    router.get("/:id", cats.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", cats.update);
  
    // Delete a Tutorial with id
    router.delete("/:id", cats.delete);
  
    // Delete all Tutorials
    router.delete("/", cats.deleteAll);
  
    app.use('/api/cats', router);
  };